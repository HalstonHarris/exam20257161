json.array!(@employees) do |employee|
  json.extract! employee, :id, :rate, :name
  json.url employee_url(employee, format: :json)
end
