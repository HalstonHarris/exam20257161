class Employee < ActiveRecord::Base
	class << self
		 def save_to_disk
		 	employees = Employee.all
		 	fname = "#{Rails.root}/test.txt"
		 	File.open(fname,mode="a") do |f|
			 	employees.each do |e|
			 		f.puts e.name + " , " + e.hourly_rate.to_s
			 	end
		 	end
		 end
		 	handle_asynchronously :save_to_disk
	end
end
